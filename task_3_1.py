def count_letter(strings, symbol):
    return sum([1 for word in strings if symbol in word])

def main():
    print(count_letter(['python', 'c', 'c++', 'kotlin', 'scala'], 'c'))


if __name__ == '__main__':
    main()